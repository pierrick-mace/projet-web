<?php
session_start();
include("include/info.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>Statistiques</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta name="generator" content="Geany 1.22" />
	<link href="css/index.css" type="text/css" rel="stylesheet"/>
</head>

<body>
	<?php
	include("include/menu.php");
	
	
	$con = mysql_connect($host, $user, $mdp)
	or die("Impossible de se connecter : " . mysql_error());

	$db_selected = mysql_select_db($table)
	or die('Impossible de sélectionner la base de données: ' . mysql_error());
	
	$query = "SELECT bonne_reponses, questions_repondues, QCM_finis FROM Projet_login WHERE login = '$_SESSION[login]'";
	
	$res = mysql_query($query);
	
	$result = mysql_fetch_array($res);
	
	echo "<div class = \"module\">";
	echo "<h1>Statistiques des QCMs:</h1>";
	echo "Bonnes reponses: ".$result['bonne_reponses']." <br />";
	echo "Questions repondues: ".$result['questions_repondues']." <br />";
	if($result['questions_repondues'] != 0)
	{
		echo "Pourcentage de bonnes reponses: ".round($result['bonne_reponses']/$result['questions_repondues']*100)."% <br />";
	}
	else echo "Pourcentage de bonnes reponses: 0% <br />";
	
	echo "QCM termines: ".$result['QCM_finis']." <br />";
	echo "</div>";
	
	?>
</body>

</html>
