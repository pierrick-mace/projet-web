<?php	
	if($_SESSION['admin'])
	{
		echo "<div class = \"navigation_admin\">";
			echo "<ul>";
				echo "<li><a href=\"index.php\">Accueil</a></li>";
				echo "<li><a href=\"stats.php\">Statistiques</a></li>";
				echo "<li><a href=\"admin.php\">Admin</a></li>";
				echo "<li><a href=\"traitement/disconnect.php\">D&eacute;connexion</a></li>";
			echo "</ul>";
		echo "</div>";
	}
	
	else
	{
		echo "<div class = \"navigation\">";
			echo "<ul>";
				echo "<li><a href=\"index.php\">Accueil</a></li>";
				echo "<li><a href=\"stats.php\">Statistiques</a></li>";
				echo "<li><a href=\"traitement/disconnect.php\">D&eacute;connexion</a></li>";
			echo "</ul>";
		echo "</div>";
	}
?>