<?php
	session_start();
	if(!empty($_SESSION['login']))
	{
		header('Location: index.php');
	}
	include("include/info.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	

<head>
	<title>Login</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta name="generator" content="Geany 1.22" />
	<link href="css/index.css" type="text/css" rel="stylesheet"/>
</head>

<body class = "login" >
	
	<div class = "module">
	<form action = "traitement/traitement_login.php" method = "post">
	<p class = "textelogin">Identifiant
	<br />
    <input type="text" name="login" />
    <br />
    </p>
    <p class = "textelogin">Mot de passe
	<br />
    <input type="password" name="mdp" />
    <br />
    <input type="submit" value="Valider" />
	</p>
    </form>
	
	<?php
		if($_GET['errno'] == 1)
		{
			echo "<p class =  \"error\">Erreur: Login ou mot de passe invalide.</p>";
		}
	?>
    
    <p class = "textelogin">Pas de compte?</p>
    <a href = "inscription.php">Inscription</a>
    </div>
    
</body>

</html>
