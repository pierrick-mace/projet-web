<?php
	session_start();
	if(!empty($_SESSION['login']))
	{
		header('Location: index.php');
	}
	include("include/info.php");
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>Inscription</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta name="generator" content="Geany 1.22" />
	<link href="css/index.css" type="text/css" rel="stylesheet"/>
</head>

<body>
	
	
	<!-- <h1>Formulaire d'inscription:</h1> -->
	<div class = "module">
	<form action = "traitement/traitement_inscription.php" method = "post">
	<p class = "textelogin">Identifiant
	<br />
    <input type="text" name="login" />
    <br />
    </p>
	
    <p class = "textelogin">Mot de passe
	<br />
    <input type="password" name="mdp" />
    <br />
    </p>
	
    <p class = "textelogin">Addresse e-mail
	<br />
    <input type="text" name="email" />
    <br />
    <input type="submit" value="Valider" />
	</p>
	</form>
	
	<?php
		
		if($_GET['errno'] == 1)
		{
			echo "<p class =  \"error\">Erreur: Veuillez renseigner tous les champs.</p>";
		}
		
		else if($_GET['errno'] == 2)
		{
			echo "<p class =  \"error\">Erreur: l'identifiant est déjà utilisé.</p>";
		}
		
		else if($_GET['errno'] == 3)
		{
			echo "<p class =  \"error\">Erreur: E-mail invalide.</p>";
		}
	?>
	</div>
</body>

</html>
